#include <stdlib.h>
#include <stdio.h>

#define ARRAY_SIZE 10

int sum_array(int *a);

int main(int argc, char const *argv[])
{
	
	int a[ARRAY_SIZE] = {0};
	int i = 0;

	while (i < ARRAY_SIZE) {
		scanf("%d\n", &(a[i]));
		i++;
	}

	int total = sum_array(a); // <- Write this function
	printf("%d", total);
	return EXIT_SUCCESS;
}


// Write a function called sum_array, which returns the sum
// of all elements in the array

