// partition.c
// gcc -Wall -Werror -std=c99 -O -o testPartition testPartition.c partition.c

//OR for gdb debugging use
// gcc -Wall -Werror -std=c99 -gdwarf-2 -o testPartition testPartition.c partition.c
     
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
     
#include "list-delete.h"
     
// Given a list and an integer, delete all nodes with the value of the integer

// So if the list is        1->5->9->60->X, 
// and the integer is       5, 
// the list should be modified to be  1->9->60->X

// If the integer is not found in the list, you dont need to do anything!

// Constraints:
// don't delete any nodes (i.e. do not call free())
// don't create any new structs (i.e. do not call malloc())
     
void delete (list l, int n) {
    link curr = l->head;
    link prev = NULL;
    while (curr != NULL) {
        link next = curr->next;
     
        if (curr->value == n){
            if (prev == NULL){
                l->head = next;
            } else {
                prev->next = next;
            }
        } else {
            prev = curr;
        }
        curr = next;
    }

}
