#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "list-delete.h"

//simple unit tests on the list
static void testList(void);
void showList (list l);

int main (int argc, char * argv[]){
	testList();
	printf("All tests passed ! You are awesome\n");
	return EXIT_SUCCESS;
}

void testList (void){

    // test a list with no nodes in it
    printf ("\nTest 1: An empty list\n");
    list l = malloc (sizeof (struct _list));
    int n = 5;
    assert (l !=NULL);
    l->head = NULL;

    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);
    assert (l->head == NULL);
	
    
    // create 10 nodes on the stack 
    node nodes[10];
 
    // test a list with one node in it
    printf ("\nTest 2: One element\n");
    l->head = &nodes[0];
    l->head->value = 42;
    l->head->next = NULL;
    n = 42;
    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);

    assert (l->head == NULL);  
    
    
    printf ("\nTest 3: Deleting the head\n");
    l->head = &nodes[0];
    l->head->value = 42;
    l->head->next = &nodes[1];
    l->head->next->value = 43;
    l->head->next->next = NULL;
    n = 42;   

    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);

    assert (l->head == &nodes[1]);  
    assert (l->head->value == 43);
    assert (l->head->next == NULL);

    printf ("\nTest 4:  Deleteing the tail\n");
    l->head = &nodes[0];
    l->head->value = 42;
    l->head->next = &nodes[1];
    l->head->next->value = 43;
    n = 43;   

    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);

    assert (l->head == &nodes[0]);  
    assert (l->head->value == 42);
    assert (l->head->next == NULL);
   
    printf ("\nTest 5:  Deleting the middle node\n");
    l->head = &nodes[0];
    nodes[0].value = 21;
    nodes[1].value = 22;
    nodes[2].value = 23;
    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next  = NULL;
    n = 22;
    
    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);
   

    assert (l->head == &nodes[0]);  
    assert (l->head->value == 21);
 
    assert (l->head->next == &nodes[2]);  
    assert (l->head->next->value == 23);
    assert (l->head->next->next == NULL); 

   
    printf ("\nTest 6:  Deleting multiple middle nodes\n");
    l->head = &nodes[0];
    nodes[0].value = 20;
    nodes[1].value = 21;
    nodes[2].value = 21;
    nodes[3].value = 23;
    n = 21;
    
    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next  = &nodes[3];
    nodes[3].next  = NULL;
    
    printf ("before delete..\n");
    printf("list is: ");
    showList(l);
    printf("n is %d\n", n);    
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
    showList(l);

    assert (l->head == &nodes[0]);  
    assert (l->head->value == 20);
 
    assert (l->head->next == &nodes[3]);  
    assert (l->head->next->value == 23);
    assert (l->head->next->next == NULL);

    printf ("\nTest 7:  Deleting every node\n");
    l->head = &nodes[0];
    nodes[0].value = 0;
    nodes[1].value = 0;
    nodes[2].value = 0;
    nodes[3].value = 0;
    nodes[4].value = 0;
    n = 0;
    
    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next  = &nodes[3];
    nodes[3].next  = &nodes[4];
    nodes[4].next  = NULL;
    
    printf ("before delete..\n");
    printf("list is: ");
        
    
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
        
    
    showList(l);
   

    assert (l->head == NULL);  

    printf ("\nTest 8:  Test not deleting any nodes\n");
    l->head = &nodes[0];
    nodes[0].value = 19;
    nodes[1].value = 20;
    nodes[2].value = 21;
    nodes[3].value = 22;
    nodes[4].value = 23;
    n = 0;
    
    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next  = &nodes[3];
    nodes[3].next  = &nodes[4];
    nodes[4].next  = NULL;
    
    printf ("before delete..\n");
    printf("list is: ");
        
    
    showList(l);
    printf("n is %d\n", n);
    delete(l, n);
    printf ("after delete\n");
    printf("list is: ");
        
    
    showList(l);
   

    assert (l->head == &nodes[0]);  
    assert (l->head->value == 19);

    assert (l->head->next == &nodes[1]);  
    assert (l->head->next->value == 20);

    assert (l->head->next->next == &nodes[2]);
    assert (l->head->next->next->value == 21);

    assert (l->head->next->next->next == &nodes[3]);
    assert (l->head->next->next->next->value == 22);

    assert (l->head->next->next->next->next == &nodes[4]);
    assert (l->head->next->next->next->next->value == 23);

    assert (l->head->next->next->next->next->next == NULL);

}

void showList (list l) {
 
    assert (l !=NULL);
 
    // start at the first node
    link current = l->head;
    while (current != NULL) {
        printf ("[%d] -> ", current->value);
        current = current->next;
    }
    printf ("X\n");
}
