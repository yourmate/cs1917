import random

for i in xrange(100):
	
	random.seed(i)
	in_file = open(str(i) + ".in", 'w')
	out_file = open(str(i) + ".in.expected", 'w')

	sum = 0
	
	for j in xrange(10):
		num = int(random.random() * 100) 
		sum += num
		in_file.write(str(num) + "\n")
	out_file.write(str(sum))
	
	in_file.close()
	out_file.close()