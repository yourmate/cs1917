#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define START_OF_GREG_CAL 1582


int isLeapYear(int year);

int main(int argc, char *argv[]) {

   int year;

   printf("Please enter your selected year:\n");
   scanf("%d", &year);
   assert(year >= START_OF_GREG_CAL);

   printf("%p\n",&year);

   int *addressOfYear = &year;
   printf("%p\n", addressOfYear);
   printf("Size of pointer: %lu\n", sizeof(addressOfYear));

   assert(isLeapYear(2000) == TRUE);

   int leapYear = isLeapYear(year);

   if (leapYear == TRUE) {
      printf("%d is a leap year!\n", year);
   } else {
      printf("%d is not a leap year!\n", year);
   }

   return EXIT_SUCCESS;
}

int isLeapYear(int year) {

   if (year % 4 == 0) {
      if (year % 100 == 0) {
         if (year % 400 == 0) {
            return TRUE;
         } else {
            return FALSE;
         }
      } else {
         return TRUE;
      }
   } else {
      return FALSE;
   }
}