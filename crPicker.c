#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define NUM_STUDENTS 5

int main(int argc, char *argv[]){
	
	srand(time(NULL));
	char* names[NUM_STUDENTS] = {"Thomas, David, Ka Wing, Xavier",
						"Daniel, Crispin, Kaveh, Josh",
						"Connor, Yan, Calandra, Vince, Vincent",
						"Luke P, Bennie, Luke T, Mingkai, Sam",
						"Cayson, Victor, Aaron, Albert"
					};

	printf("I wonder who will give the code review this week!\n");

	int number = rand() % NUM_STUDENTS;

	assert(number < NUM_STUDENTS);
	printf("%s\n", names[number]);

	return EXIT_SUCCESS;
}