#include <stdio.h>
#include <stdlib.h>

#define ALPHABET_SIZE 26

void processChar(char c, int *letterCount);

int main(int argc, char *argv[]){

	int letterCount[ALPHABET_SIZE] = {0};
	unsigned int charsEntered = 0;
	char currentChar;

	printf("Letter count is: %p\n", letterCount);

	currentChar = getchar();

	while (currentChar != EOF){
		processChar(currentChar, letterCount);
		charsEntered++;
		currentChar = getchar();
	}

	int i = 0;
	while (i < ALPHABET_SIZE) {
		printf("%c, %d\n", ('a' + i), letterCount[i]);
		i++;
	}

	return EXIT_SUCCESS;
}

void processChar(char c, int letterCount[]) {
	letterCount[c - 97]++;
}

char *string = "Hello\0";
// string[0] = 'H'
// string[1] = 'e'


// char string[5];
// string[2] = 'f'





