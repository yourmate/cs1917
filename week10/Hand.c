#include "Hand.h"
#include <stdlib.h>
typedef struct _hand {
	card *c;
} hand;

Hand createHand(int maxSize){
	Hand h = malloc(sizeof(struct _hand));

	h->c = malloc(sizeof(struct _card) * maxSize);	
	return h;
}

void destroyHand(Hand h){
	free(h->c);
	free(h);
}