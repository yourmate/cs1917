#define NONE 0;
#define DIAMONDS 1;
#define HEARTS 2;
//etc

typedef struct _hand *Hand;

typedef struct _card{
     int suit;
     int rank;
}card;

//Create an empty hand of Cards with a max size
Hand createHand(int maxSize);
void destroyHand(Hand h);
//etc other functions to deal cards etc