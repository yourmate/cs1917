#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct _node *link;

typedef struct _node { 
    int value; 
    link next; 
} node;

typedef struct _list *list;

struct _list { 
    link first; 
};

void insert(list ls, int num);

int length(list ls);

int areEqual(list ls1, list ls2);

list copy(list ls);

list zip(list ls1, list ls2);

void minToFront(list ls);

void printList(list ls);

int main(int argc, char *argv[]){

	list l = malloc(sizeof(struct _list));
	l->first = NULL;
	insert(l, 100);
	insert(l, 2);
	insert(l, 9);
	printList(l);

	list m = malloc(sizeof(struct _list));
	m->first = NULL;

	insert(m, 1);
	insert(m, 5);
	insert(m, 8);
	insert(m, 2);
	insert(m, 1);
	printList(m);

	list z = zip(l,m);
	printList(z);

	return EXIT_SUCCESS;
}

int length(list ls){
	int len = 0;

	link curr = ls->first;

	while (curr != NULL) {
		len++;
		curr = curr->next;
	}

	return len;
}


void insert(list ls, int num) {
	link new = malloc(sizeof(struct _node));
	new->value = num;
	new->next = NULL;

	link curr = ls->first;

	if (curr == NULL){
		ls->first = new;
	} else {
		while(curr->next != NULL){
			curr = curr->next;
		}
		curr->next = new;
	}
}

void minToFront(list ls){
	link curr = ls->first;
	link prev = NULL;

	int min = curr->value;
	link minNode = curr;
	link minPrev = NULL;

	while (curr != NULL) {
		if (curr->value < min) {
			min = curr->value;
			minNode = curr;
			minPrev = prev;
		}
		prev = curr;
		curr = curr->next;
	}

	if (minPrev != NULL){
		minPrev->next = minNode->next;
	}

	minNode->next = ls->first;
	ls->first = minNode;

}

int areEqual(list l1, list l2){
	int result = TRUE;
	
	if (length(l1) != length(l2)) {
		result = FALSE;
	}

	link l1curr = l1->first;
	link l2curr = l2->first;
		
	while(l1curr != NULL && result != FALSE){
		if (l1curr->value != l2curr->value){
			result = FALSE;
		}
		l1curr = l1curr->next;
		l2curr = l2curr->next;
	}


	return result;
}


list copy(list l){
	list new = malloc(sizeof(struct _list));
	new->first = NULL;

	link curr = l->first;
	while (curr != NULL) {
		insert(new, curr->value);
		curr = curr->next;
	}
	return new;
}

list zip(list l1, list l2) {
	link l1curr = l1->first;
	link l2curr = l2->first;
	list new = malloc(sizeof(struct _list));
	new->first = NULL;
	link lastOfNew = NULL;

	while (l1curr != NULL || l2curr != NULL){
		
		if (l1curr != NULL){

			if (new->first == NULL){
				new->first = l1curr;
			} else {
				lastOfNew->next = l1curr;
			}
			lastOfNew = l1curr;
			l1curr = l1curr->next;
		}

		if (l2curr != NULL) {
			if (new->first == NULL){
				new->first = l2curr;
			} else {
				lastOfNew->next = l2curr;
			}
			lastOfNew = l2curr;
			l2curr = l2curr->next;			
		}

	}

	l1->first = NULL;
	l2->first = NULL;
	return new;
}



void printList(list ls) {
	link curr = ls->first;
	while(curr != NULL) {
		printf("[%d] -> ", curr->value);
		curr = curr->next;
	}
	printf("X\n");
	printf("This list is %d elements long\n", length(ls));
}